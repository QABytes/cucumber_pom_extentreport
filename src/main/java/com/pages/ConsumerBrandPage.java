package com.pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

public class ConsumerBrandPage {
	private WebDriver driver;
	@FindBy (xpath="//div[@class='brand-listing-title']/h2")
	@CacheLookup
	List<WebElement> allLinks;
	
	 public ConsumerBrandPage (WebDriver driver){
	        this.driver = driver;
	    }
	 
	 public String PageTitle(){
	        return driver.getTitle();
	    }
	 
	 public String allLinks(){
		 	List<WebElement> all_Links = driver.findElements(By.xpath("//div[@class='brand-listing-title']/h2"));
		 	List<String> li = new ArrayList<String>();
			for (WebElement a : all_Links){
	            System.out.println(a.getText());
	            
	            li.add(a.getText());
	        }
			return li.toString();
	    }
	    
	 

}
