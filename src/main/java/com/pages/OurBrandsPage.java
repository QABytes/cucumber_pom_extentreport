package com.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

public class OurBrandsPage {
	private WebDriver driver;

    //Locators
    private By consumerbrands = By.xpath("(//li/a[contains(text(),'Consumer Brands')])[2]");
    private By BusinesstoBusinessAreas = By.xpath("//li/a[text()='Business to Business Areas']");
    private By ProductSafety = By.xpath("(//li/a[text()='Product Safety'])[2]");
    private By ProductIngredientDisclosure = By.xpath("(//li/a[text()='Product Ingredient Disclosure'])[2]");
    private By GHSSafety = By.xpath("(//li/a[text()='GHS Safety'])[2]");
    private By BrandNewsletterSignup = By.xpath("(//li/a[text()='Brand Newsletter Signup'])[2]");
    
    public OurBrandsPage (WebDriver driver){
        this.driver = driver;
    }
    public String PageTitle(){
        return driver.getTitle();
    }
    
    public void clickconsumerbrands(){
    	JavascriptExecutor je = (JavascriptExecutor)driver;
    	je.executeScript("arguments[0].click();",driver.findElement(consumerbrands));

    //    driver.findElement(consumerbrands).click();
    }
    
    public void clickProductSafety(){
        driver.findElement(ProductSafety).click();
    }
    
    public void clickBusinesstoBusinessAreas(){
        driver.findElement(BusinesstoBusinessAreas).click();
    }
    

    public void clickProductIngredientDisclosure(){
        driver.findElement(ProductIngredientDisclosure).click();
    }
    
    public void clickGHSSafety(){
        driver.findElement(GHSSafety).click();
    }
    
    public void clickBrandNewsletterSignup(){
        driver.findElement(BrandNewsletterSignup).click();
    }
    
}
