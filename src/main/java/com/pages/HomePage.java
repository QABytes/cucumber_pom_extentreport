package com.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class HomePage {

    private WebDriver driver;

    //Locators
    private By Username = By.xpath("//input[@id='txtUsername']");
    private By Company = By.xpath("//a[@href='/company/']");
    private By Ourbrands = By.xpath("//a[@href='/our-brands/']");
    private By Responsibility = By.xpath("//a[@href='/responsibility/']");
    private By Careers = By.xpath("//a[@href='/careers/']");
    private By Innovation = By.xpath("//a[@href='/innovation/']");
    private By Investors = By.xpath("//a[contains(text(),'Investors') and @role='button'])");
    private By LatestNews = By.xpath("///li/a[text()='Latest News']");
    private By International = By.xpath("//li/a[text()='International']");
      

    //constructor
    public HomePage (WebDriver driver){
        this.driver = driver;
    }

    public void launchURL(String url){
        driver.get(url);
    }

    public String getHomePageTitle(){
        return driver.getTitle();
    }

    public void clickUsername(){
        driver.findElement(Username).click();
    }
    
    public void clickCompany(){
        driver.findElement(Company).click();
    }
    public void clickOurbrands(){
        driver.findElement(Ourbrands).click();
    }
    public void clickResponsibility(){
        driver.findElement(Responsibility).click();
    }
    
    public void clickCareers(){
        driver.findElement(Careers).click();
    }
    public void clickInnovation(){
        driver.findElement(Innovation).click();
    }
    
    public void clickInvestors(){
        driver.findElement(Investors).click();
    }
    
    public void clickLatestNews(){
        driver.findElement(LatestNews).click();
    }
    public void clickInternational(){
        driver.findElement(International).click();
    }
}
