package StepDefns;

import com.pages.ConsumerBrandPage;
import com.pages.HomePage;
import com.pages.OurBrandsPage;
import com.qa.factory.DriverFactory;
import io.cucumber.java.en.*;
import org.junit.Assert;

public class Steps {

	private HomePage hPage = new HomePage(DriverFactory.getDriver());
	private OurBrandsPage obPage = new OurBrandsPage(DriverFactory.getDriver());
	private ConsumerBrandPage cbPage = new ConsumerBrandPage(DriverFactory.getDriver());
	

	@Given("the user launches URL")
	public void the_user_launches_url() {

		//   DriverFactory.getDriver().get();
		hPage.launchURL(ApplnHooks.prop.getProperty("url"));
	}


	@Then("the user verifies homepage is displayed")
	public void the_user_verifies_homepage_is_displayed() {
		
		Assert.assertTrue(hPage.getHomePageTitle().contains("Church and Dwight | Consumer Goods | Home and Personal Care Products"));
	}



	@When("the user clicks on Our Brand page")
	public void the_user_clicks_on_our_brand_page() {
		hPage.clickOurbrands();
	}
	@Then("the user verifies OurBrand page is displayed")
	public void the_user_verifies_our_brand_page_is_displayed() {
		Assert.assertTrue(obPage.PageTitle().contains("Church & Dwight Brands | Home, Health, Fabric & Personal Care Products - Our Brands"));
	}
	@Then("the user clicks on Consumer Brand link")
	public void the_user_clicks_on_consumer_brand_link() {
		obPage.clickconsumerbrands();
		Assert.assertTrue(obPage.PageTitle().contains("Consumer Brands | Fabric, Health, Home, and Personal Care - Our Brands - Consumer Brands"));
		
	}
	@Then("the user verifies Header links")
	public void the_user_verifies_header_links() {
		cbPage.allLinks();
	}


}
