Feature: Churchdwight Consumer Brand Page
  Scenario: To verify header links in churchdwight

    Given the user launches URL
   	Then the user verifies homepage is displayed
   	When the user clicks on Our Brand page
   	Then the user verifies OurBrand page is displayed
   	And the user clicks on Consumer Brand link
   	And the user verifies Header links
   	